<?php

defined('MOODLE_INTERNAL') || die();

/* Marketing Spot Settings temp*/
$page = new admin_settingpage('theme_ffocus_marketing', get_string('marketingheading', 'theme_ffocus'));

// Toggle FP Textbox Spots.
$name = 'theme_ffocus/togglemarketing';
$title = get_string('togglemarketing' , 'theme_ffocus');
$description = get_string('togglemarketing_desc', 'theme_ffocus');
$displaytop = get_string('displaytop', 'theme_ffocus');
$displaybottom = get_string('displaybottom', 'theme_ffocus');
$default = '2';
$choices = array('1'=>$displaytop, '2'=>$displaybottom);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot One
$name = 'theme_ffocus/marketing1info';
$heading = get_string('marketing1', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot One
$name = 'theme_ffocus/marketing1';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing1image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing1image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing1content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing1buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing1buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing1target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Two
$name = 'theme_ffocus/marketing2info';
$heading = get_string('marketing2', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Two.
$name = 'theme_ffocus/marketing2';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing2image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing2image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing2content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing2buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing2buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing2target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Three
$name = 'theme_ffocus/marketing3info';
$heading = get_string('marketing3', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Three.
$name = 'theme_ffocus/marketing3';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing3image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing3image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing3content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing3buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing3buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing3target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_ffocus/marketing4info';
$heading = get_string('marketing4', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_ffocus/marketing4';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing4image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing4image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing4content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing4buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing4buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing4target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_ffocus/marketing5info';
$heading = get_string('marketing5', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_ffocus/marketing5';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing5image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing5image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing5content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing5buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing5buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing5target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_ffocus/marketing6info';
$heading = get_string('marketing6', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_ffocus/marketing6';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing6image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing6image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing6content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing6buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing6buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing6target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_ffocus/marketing7info';
$heading = get_string('marketing7', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Seven
$name = 'theme_ffocus/marketing7';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing7image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing7image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing7content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing7buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing7buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing7target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_ffocus/marketing8info';
$heading = get_string('marketing8', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Eight
$name = 'theme_ffocus/marketing8';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing8image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing8image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing8content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing8buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing8buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing8target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_ffocus/marketing9info';
$heading = get_string('marketing9', 'theme_ffocus');
$information = get_string('marketinginfodesc', 'theme_ffocus');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Nine
$name = 'theme_ffocus/marketing9';
$title = get_string('marketingtitle', 'theme_ffocus');
$description = get_string('marketingtitledesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_ffocus/marketing9image';
$title = get_string('marketingimage', 'theme_ffocus');
$description = get_string('marketingimage_desc', 'theme_ffocus');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing9image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing9content';
$title = get_string('marketingcontent', 'theme_ffocus');
$description = get_string('marketingcontentdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing9buttontext';
$title = get_string('marketingbuttontext', 'theme_ffocus');
$description = get_string('marketingbuttontextdesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing9buttonurl';
$title = get_string('marketingbuttonurl', 'theme_ffocus');
$description = get_string('marketingbuttonurldesc', 'theme_ffocus');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_ffocus/marketing9target';
$title = get_string('marketingurltarget' , 'theme_ffocus');
$description = get_string('marketingurltargetdesc', 'theme_ffocus');
$target1 = get_string('marketingurltargetself', 'theme_ffocus');
$target2 = get_string('marketingurltargetnew', 'theme_ffocus');
$target3 = get_string('marketingurltargetparent', 'theme_ffocus');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Must add the page after definiting all the settings!
$settings->add($page);